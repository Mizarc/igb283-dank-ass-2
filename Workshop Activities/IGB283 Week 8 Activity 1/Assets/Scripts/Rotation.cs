﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    Mesh mesh;
    public Vector3 angle;

    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] vertices = mesh.vertices;
        
        Matrix4x4 R = Rotate(angle * Time.deltaTime);
        Matrix4x4 M = R;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
            Debug.Log(vertices[i]);
        }

        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }

    // Rotation Matrix
    public Matrix4x4 Rotate(Vector3 angle)
    {
        // Create a new matrix
        Matrix4x4 matrix = new Matrix4x4();
        Matrix4x4 matrixX = new Matrix4x4();
        Matrix4x4 matrixY = new Matrix4x4();
        Matrix4x4 matrixZ = new Matrix4x4();

        // Set the rows of the matrix
        matrixX.SetRow(0, new Vector4(1.0f, 0.0f, 0.0f, 0.0f));
        matrixX.SetRow(1, new Vector4(0.0f, Mathf.Cos(angle[0]), -Mathf.Sin(angle[0]), 0.0f));
        matrixX.SetRow(2, new Vector4(0.0f, Mathf.Sin(angle[0]), Mathf.Cos(angle[0]), 0.0f));
        matrixX.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f));

        matrixY.SetRow(0, new Vector4(Mathf.Cos(angle[1]), 0.0f, Mathf.Sin(angle[1]), 0.0f));
        matrixY.SetRow(1, new Vector4(0.0f, 1.0f, 0.0f, 0.0f));
        matrixY.SetRow(2, new Vector4(-Mathf.Sin(angle[1]), 0.0f, Mathf.Cos(angle[1]), 0.0f));
        matrixY.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f));

        matrixZ.SetRow(0, new Vector4(Mathf.Cos(angle[2]), -Mathf.Sin(angle[2]), 0.0f, 0.0f));
        matrixZ.SetRow(1, new Vector4(Mathf.Sin(angle[2]), Mathf.Cos(angle[2]), 0.0f, 0.0f));
        matrixZ.SetRow(2, new Vector4(0.0f, 0.0f, 1.0f, 0.0f));
        matrixZ.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f));

        matrix = matrixX * matrixY * matrixZ;
        return matrix;

    }
}
