﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curve : MonoBehaviour
{
    private float FindY (float x)
    {
        return -Mathf.Pow(x / 3, 2) + 4;
    }

    public int numberDivisions = 31;
    public float domainMin = -7.0f;
    public float domainMax = 7.0f;
    public float width = 0.1f;

    //Area variables
    public Material fillMaterial;
    public Material rectangleMaterial;

    public int numberRectangles = 5;
    public int numberFillVertices = 7;
    public float x1 = -2.0f;
    public float x2 = 3.0f;
    public Color areaFillColor = new Color(0.8f, 0.3f, 0.3f, 1.0f);
    public Color rectangleColor = new Color(0.2f, 0.6f, 0.3f, 0.5f);

    public GameObject areaFill;
    public GameObject rectangles;

    private float areaUnderCurve = 0.0f;

    private void DrawCurve()
    {
        LineRenderer lr = GetComponent<LineRenderer>();
        lr.positionCount = numberDivisions;
        lr.widthMultiplier = width;

        float incrementSize = (Mathf.Abs(domainMin) + Mathf.Abs(domainMax)) / (numberDivisions - 1);
        for(int i = 0; i < numberDivisions; i++)
        {
            Vector3 position = new Vector3();

            position.x = ((float)i * incrementSize) + domainMin;
            position.y = FindY(position.x);
            position.z = 0.0f;

            lr.SetPosition(i, position);
        }
    }

    private void FillArea()
    {
        areaFill.AddComponent<MeshFilter>();
        areaFill.AddComponent<MeshRenderer>();

        Mesh mesh = areaFill.GetComponent<MeshFilter>().mesh;

        areaFill.GetComponent<MeshRenderer>().material = fillMaterial;

        mesh.Clear();

        // Find the higher y Value
        float lowerY = 0;

        float y1 = FindY(x1);
        float y2 = FindY(x2);

        if (y1 > y2)
        {
            lowerY = y1;
        }
        else
        {
            lowerY = y1;
        }

        if (lowerY < 0)
        {
            throw new System.Exception("This implementation does not support finding the area of the curve");
        }

        List<Vector3> vertices = new List<Vector3>()
        {
            new Vector3(x1, 0, 0),
            new Vector3(x1, lowerY, 0),
            new Vector3(x2, 0, 0),
            new Vector3(x2, lowerY, 0)
        };

        List<Color> colors = new List<Color>()
        {
            areaFillColor,
            areaFillColor,
            areaFillColor,
            areaFillColor
        };

        List<int> triangles = new List<int>() {0, 1, 2, 1, 2, 3 };

        float incrementSize = (Mathf.Abs(x2) + Mathf.Abs(x1)) / (numberFillVertices - 1);
        int currentIndex = vertices.Count;

        triangles.Add(1);
        triangles.Add(4);
        triangles.Add(5);
        for(int i = 0; i < numberFillVertices; i++)
        {
            Vector3 v1 = new Vector3();

            v1.x = ((float) i * incrementSize) + x1;
            v1.y = FindY(v1.x);
            v1.z = 0.0f;

            Vector3 v2 = new Vector3();

            v2.x = v2.x;
            v2.y = lowerY;
            v2.z = 0.0f;

            vertices.Add(v1);
            vertices.Add(v2);

            colors.Add(areaFillColor);
            colors.Add(areaFillColor);

            triangles.Add(currentIndex);
            triangles.Add(currentIndex + 1);
            triangles.Add(currentIndex + 2);
            triangles.Add(currentIndex + 1);
            triangles.Add(currentIndex + 2);
            triangles.Add(currentIndex + 3);

            currentIndex += 2;

        }


        Vector3 lv1 = new Vector3();

        lv1.x = x2;
        lv1.y = FindY(lv1.x);
        lv1.z = 0.0f;

        Vector3 lv2 = new Vector3();

        lv2.x = x2;
        lv2.y = lowerY;
        lv2.z = 0.0f;

        vertices.Add(lv1);
        vertices.Add(lv2);

        colors.Add(areaFillColor);
        colors.Add(areaFillColor);

        mesh.vertices = vertices.ToArray();
        mesh.colors = colors.ToArray();
        mesh.triangles = triangles.ToArray();



    }

    public void DrawRectangles()
    {
        rectangles.AddComponent<MeshFilter>();
        rectangles.AddComponent<MeshRenderer>();

        Mesh mesh = rectangles.GetComponent<MeshFilter>().mesh;

        rectangles.GetComponent<MeshRenderer>().material = rectangleMaterial;

        mesh.Clear();

        List<Vector3> vertices = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<int> triangles = new List<int>();

        float increamentSize = (Mathf.Abs(x2) + Mathf.Abs(x1)) / (numberRectangles);
        
        for (int i = 0; i < numberRectangles; i++)
        {
            Vector3 v1 = new Vector3();

            v1.x = ((float) i * increamentSize) + x1;
            v1.y = 0.0f;
            v1.z = -0.1f;

            float y1 = FindY(v1.x);
            float y2 = FindY(v1.x + increamentSize);

            float smallerY = y1 < y2 ? y1 : y2;

            Vector3 v2 = new Vector3();

            v2.x = v1.x;
            v2.y = smallerY;
            v2.z = -0.1f;

            Vector3 v3 = new Vector3();

            v3.x = v1.x + increamentSize;
            v3.y = 0.0f;
            v3.z = -0.1f;

            Vector3 v4 = new Vector3();

            v4.x = v1.x + increamentSize;
            v4.y = smallerY;
            v4.z = -0.1f;

            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);
            vertices.Add(v4);

            colors.Add(rectangleColor);
            colors.Add(rectangleColor);
            colors.Add(rectangleColor);
            colors.Add(rectangleColor);

            triangles.Add(i * 4);
            triangles.Add(i * 4 + 1);
            triangles.Add(i * 4 + 2);
            triangles.Add(i * 4 + 1);
            triangles.Add(i * 4 + 2);
            triangles.Add(i * 4 + 3);

            areaUnderCurve += increamentSize * smallerY;
        }

        mesh.vertices = vertices.ToArray();
        mesh.colors = colors.ToArray();
        mesh.triangles = triangles.ToArray();

    }


    // Start is called before the first frame update
    void Start()
    {
        DrawCurve();
        FillArea();
        DrawRectangles();
        Debug.Log("areaUnderCurve");
    }
}
