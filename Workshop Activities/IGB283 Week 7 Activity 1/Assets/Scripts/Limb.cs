﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IGB283;

public class Limb : MonoBehaviour
{
    public GameObject child;

    public GameObject control;

    public Vector3 jointLocation;
    public Vector3 jointOffset;

    public float angle;

    public float lastAngle;

    public Vector3[] limbVertexLocations;

    public Mesh mesh;

    public Material material;


    //Draw Function 
    public void DrawLimb()
    {
        
        //Mesh =  Mesh
        //Mesh Render Add

        //Add MeshFitler & MeshRenderer
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        mesh = GetComponent<MeshFilter>().mesh;
        GetComponent<MeshRenderer>().material = material;

        mesh.vertices = limbVertexLocations;

        mesh.triangles = new int[] { 0, 1, 2, 0, 3, 2};
        
    }

    public void MoveByOffset (Vector3 offset)
    {
        Matrix3x3 T = Translate(offset);

        Vector3[] vertices = mesh.vertices;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = T.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;

        jointLocation = T.MultiplyPoint(jointLocation);

        if (child != null)
        {
            child.GetComponent<Limb>().MoveByOffset(offset);
        }
    }

    void Awake()
    {
        DrawLimb();
    }

    void Start()
    {
        if (child != null)
        {
            child.GetComponent<Limb>().
                MoveByOffset(jointOffset);
        }
    }

    public void RotateAroundPoint(Vector3 point, float angle, float lastAngle)
    {
        Matrix3x3 T1 = Translate(-point);

        Matrix3x3 R1 = Rotate(-lastAngle);

        Matrix3x3 T2 = Translate(point);

        Matrix3x3 R2 = Rotate(angle);

        Matrix3x3 M = T2 * R2 * R1 * T1;

        //Move the mesh
        Vector3[] vertices = mesh.vertices;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;

        jointLocation = M.MultiplyPoint(jointLocation);

        if (child != null)
        {
            child.GetComponent<Limb>().RotateAroundPoint(point, angle, lastAngle);
        }

    }

    public void Update()
    {
        lastAngle = angle;
        if (control != null)
        {
            angle = control.GetComponent<Slider>().value;
        }

        if (child != null)
        {
            child.GetComponent<Limb>().RotateAroundPoint(jointLocation, angle, lastAngle);
        }

        mesh.RecalculateBounds();
    }


    public static Matrix3x3 Rotate (float angle)
    {
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(Mathf.Cos(angle), -Mathf.Sin(angle), 0.0f));
        matrix.SetRow(1, new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0.0f));
        matrix.SetRow(2, new Vector3(0.0f, 0.0f, 1.0f));

        return matrix;

    }

    public static Matrix3x3 Translate (Vector3 offset)
    {
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(1.0f, 0.0f, offset.x));
        matrix.SetRow(1, new Vector3(0.0f, 1.0f, offset.y));
        matrix.SetRow(2, new Vector3(0.0f, 0.0f, 1.0f));

        return matrix;
    }



}
