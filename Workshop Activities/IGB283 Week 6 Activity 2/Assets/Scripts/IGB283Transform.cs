﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGB283;

public class IGB283Transform : MonoBehaviour
{
    public void Test()
    {
        Debug.Log("ping");
    }

    // Rotation Matrix
    public Matrix3x3 Rotate(float angle)
    {
        // Create a new matrix
        Matrix3x3 matrix = new Matrix3x3();

        // Set the rows of the matrix
        matrix.SetRow(0, new Vector3(Mathf.Cos(angle), -Mathf.Sin(angle), 0.0f));
        matrix.SetRow(1, new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0.0f));
        matrix.SetRow(2, new Vector3(0.0f, 0.0f, 1.0f));

        return matrix;

    }
    public Matrix3x3 Translate(Vector3 transform){
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(1, 0, transform.x));
        matrix.SetRow(1, new Vector3(0, 1, transform.y));
        matrix.SetRow(2, new Vector3(0, 0, 1));

        return matrix;
    }

    public Matrix3x3 Scale(float scale){
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(1 + scale, 0, 0));
        matrix.SetRow(1, new Vector3(0, 1 + scale, 0));
        matrix.SetRow(2, new Vector3(0, 0, 1));

        return matrix;
    }
}
