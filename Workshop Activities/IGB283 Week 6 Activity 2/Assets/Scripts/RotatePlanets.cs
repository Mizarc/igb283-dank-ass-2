﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IGB283;

public class RotatePlanets : MonoBehaviour
{
    public float distance;
    public float size;
    public float orbitSpeed;
    public float rotateSpeed;
    public bool isMoon;
    public Vector3 centerPoint;
    private Mesh mesh;
    private IGB283Transform cTransform;

    // Start is called before the first frame update
    void Start()
    {
        cTransform = gameObject.GetComponent<IGB283Transform>();
        mesh = gameObject.GetComponent<MeshFilter>().mesh;   

        Vector3[] vertices = mesh.vertices;

        Matrix3x3 T = cTransform.Translate(new Vector3(distance,0,0));
        Matrix3x3 S = cTransform.Scale(size);

        Matrix3x3 M = T * S;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoon == true)
        {
            Vector3 parentPoint = gameObject.transform.parent.GetComponent<RotatePlanets>().centerPoint;
            RotatePlanet(parentPoint, rotateSpeed, orbitSpeed);
        }
        else
        {
            RotatePlanet(mesh.bounds.center, rotateSpeed, orbitSpeed);
        }
        
        
    }

    void RotatePlanet(Vector3 point, float rotateSpeed, float orbitSpeed)
    {
        Vector3[] vertices = mesh.vertices;
        
        Matrix3x3 T1 = cTransform.Translate(point);
        Matrix3x3 R1 = cTransform.Rotate(rotateSpeed * Time.deltaTime);
        Matrix3x3 T2 = cTransform.Translate(-point);
        Matrix3x3 R2 = cTransform.Rotate(orbitSpeed * Time.deltaTime / 10);
        
        Matrix3x3 M = R2 * T1 * R1 * T2;
        
        
        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
        }

        
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
    }
}
