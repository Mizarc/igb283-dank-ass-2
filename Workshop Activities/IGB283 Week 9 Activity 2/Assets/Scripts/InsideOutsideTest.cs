﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InsideOutsideTest : MonoBehaviour
{
    public GameObject point;
    public GameObject polygon;
    public GameObject text;

    private LineRenderer lr;
    private Polygon p;
    private Text t;

    // Start is called before the first frame update
    void Start()
    {
        p = polygon.GetComponent<Polygon>();

        lr = this.GetComponent<LineRenderer>();
        lr.widthMultiplier = 0.05f;
        lr.positionCount = 2;

        t = text.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        List<Edge> edges = new List<Edge>();
        for (int i = 0; i < p.numVertices; i++)
        {
            int j = i % p.numVertices;
            int k = (i + 1) % p.numVertices;

            Edge edge = new Edge((Vector2)
            p.vertices[j].transform.position, (Vector2)
            p.vertices[k].transform.position);

            edges.Add(edge);
        }

        float minX = float.MaxValue;
        float maxX = float.MinValue;

        foreach (GameObject vertex in p.vertices)
        {
            if (vertex.transform.position.x < minX)
            {
                minX = vertex.transform.position.x;
            }
            else if (vertex.transform.position.x > maxX)
            {
                maxX = vertex.transform.position.x;
            }
        }

        float e = ((maxX - minX) / 100.0f);

        Vector2 rayStart = new Vector2(minX - e, 0.0f);
        Vector2 rayEnd = (Vector2) point.transform.position;

        Edge ray = new Edge(rayStart, rayEnd);
        lr.SetPosition(0, rayStart);
        lr.SetPosition(1, rayEnd);

        int numCrossings = 0;
        for (int i = 0; i < edges.Count; i++)
        {
            if(edges[i].CollidesWith(ray))
            {
                numCrossings++;
            }
        }

        if (numCrossings % 2 == 0)
        {
            Debug.Log("Outside");
            t.text = "Outside";
        }
        else
        {
            Debug.Log("Inside");
            t.text = "Inside";
        }
    }
}
