﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Polygon : MonoBehaviour
{
    public GameObject vertex;
    public int numVertices = 10;
    public float startingRadius = 2;

    public List<GameObject> vertices;
    private LineRenderer lr;

    private const float tau = 6.2832f;

    // Start is called before the first frame update
    void Start()
    {
        float angle = tau / numVertices;
        for (int i = 0; i < numVertices; i++)
        {
            Vector3 position = new Vector3();
            position.x = startingRadius * Mathf.Sin(angle * i);
            position.y = startingRadius * Mathf.Cos(angle * i);

            GameObject vertexInstance = Instantiate(vertex, this.transform);
            vertexInstance.transform.position = position;
            vertices.Add(vertexInstance);
        }

        lr = this.GetComponent<LineRenderer>();
        lr.widthMultiplier = 0.05f;
        lr.positionCount = numVertices + 1;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i <= numVertices; i++)
        {
            int j = i % numVertices;
            lr.SetPosition(i, vertices[j].transform.position);
        }
    }
}
