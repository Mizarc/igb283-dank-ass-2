﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindIntersection : MonoBehaviour
{
    // GameObjects used in calculation 
    public GameObject plane;
    public GameObject line;
    public GameObject intersection;

    // Plane variables 
    public Vector3[] planeCorners;
    public Material planeMaterial;
    private Mesh mesh;

    // Line variables 
    public Vector3[] lineEnds;
    public float lineWidth;
    private LineRenderer lr;

    // Start is called before the first frame update
    void Start()
    {
        // Draw the plane 
        SetupPlane();

        SetupLine();

        FindIntersectionLocation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SetupPlane()
    {
        // Add a MeshFilter and MeshRenderer to the Empty GameObject 
        plane.AddComponent<MeshFilter>();
        plane.AddComponent<MeshRenderer>();
        // Get the Mesh from the MeshFilter 
        mesh = plane.GetComponent<MeshFilter>().mesh;
        // Set the material to the material we have selected 
        plane.GetComponent<MeshRenderer>().material = planeMaterial;
        // Clear all vertex and index data from the mesh 
        mesh.Clear();

        // Create a plane 
        mesh.vertices = new Vector3[] {
            planeCorners[0],
            planeCorners[1],
            planeCorners[2],
            planeCorners[3]
        };
        // Set the colour of the rectangle 
        mesh.colors = new Color[] {
            new Color(0.8f, 0.8f, 0.8f, 1.0f),
            new Color(0.8f, 0.8f, 0.8f, 1.0f),
            new Color(0.8f, 0.8f, 0.8f, 1.0f),
            new Color(0.8f, 0.8f, 0.8f, 1.0f)
        };
        // Set vertex indicies 
        mesh.triangles = new int[]{0, 1, 2, 0, 2, 3};
}
    void SetupLine()
    {
        // Get the linerenderer from the gameobject 
        lr = line.GetComponent<LineRenderer>();
        // Set the width of the line 
        lr.widthMultiplier = lineWidth;
        // Set the number of positions 
        lr.positionCount = 2;
        // Set the positions of the ends of the line 
        lr.SetPosition(0, lineEnds[0]);
        lr.SetPosition(1, lineEnds[1]);
    }

    void FindIntersectionLocation()
    {
        // Get the mesh data 
        Vector3[] vertices = mesh.vertices;
        int[] triangles = mesh.triangles;

        // Find the relevant points 
        Vector3 p0 = vertices[triangles[1]];
        Vector3 p1 = vertices[triangles[0]];
        Vector3 p2 = vertices[triangles[2]];
        Vector3 la = lineEnds[0];
        Vector3 lb = lineEnds[1];

        // Find the matrix 
        Matrix4x4 M = new Matrix4x4();
        M.SetRow(0, new Vector4(la.x - lb.x, p1.x - p0.x, p2.x - p0.x, 0.0f));
        M.SetRow(1, new Vector4(la.y - lb.y, p1.y - p0.y, p2.y - p0.y, 0.0f));
        M.SetRow(2, new Vector4(la.z - lb.z, p1.z - p0.z, p2.z - p0.z, 0.0f));
        M.SetRow(3, new Vector4(0.0f, 0.0f, 0.0f, 1.0f)); M = M.inverse;
        // Find the vector 
        Vector3 vector = new Vector3(la.x - p0.x, la.y - p0.y, la.z - p0.z);

        // Find the value of t, u and v 
        Vector3 tuv = M.MultiplyPoint(vector);
        float t = tuv.x;
        float u = tuv.y;
        float v = tuv.z;

        // Show that the point is on the line and the plane 
        bool onLine = (t <= 1 && t >= 0);
        bool onPlane = (u <= 1 && u >= 0) && (v <= 1 && v >= 0);

        // Set location if on line and plane 
        if (onLine && onPlane)
        {
            intersection.transform.position = la + (lb - la) * t;
        }
        else
        {
            Debug.Log("The line and plane to not intersect.");
        }
    }
}
