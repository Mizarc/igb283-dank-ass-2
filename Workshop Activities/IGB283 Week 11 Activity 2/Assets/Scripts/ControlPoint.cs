﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPoint : MonoBehaviour
{
    public bool isMoving = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            Vector3 position = new Vector3();
            // Find the mouse position in word space 
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // Find the new position of the gameobject 
            position.x = mousePosition.x;
            position.y = mousePosition.y;
            position.z = transform.position.z;
            transform.position = position;

        }
    }
}
