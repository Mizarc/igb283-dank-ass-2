﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge
{
    public Vector2 start;
    public Vector2 end;

    public Vector2 displacement {get {return end - start;} }

    public Edge (Vector2 start, Vector2 end)
    {
        this.start = start;
        this.end = end;
    }

    private float Cross2D (Vector2 v1, Vector2 v2)
    {
        return v1.x * v2.y - v1.y * v2.x;
    }

    public static bool operator== (Edge e1, Edge e2)
    {
        return (e1.start == e2.start && e1.end == e2.end) || (e1.start == e2.end && e1.end == e2.start);
    }

    public static bool operator!= (Edge e1, Edge e2)
    {
        return !(e1 == e2);
    }

    public override bool Equals(object obj)
    {
        return this == (Edge) obj;
    }

    public override int GetHashCode()
    {
        return start.GetHashCode() & end.GetHashCode();
    }

    public bool CollidesWith (Edge ray)
    {
        float t;
        float u;

        t = Cross2D((ray.start - this.start), ray.displacement) / Cross2D(this.displacement, ray.displacement);
        u = Cross2D((ray.start - this.start), this.displacement) / Cross2D(this.displacement, ray.displacement);

        if (t <= 1 && t >= 0 
        && u <= 1 && u >= 0 
        && Cross2D(this.displacement, ray.displacement) != 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
