﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation2 : MonoBehaviour
{
    List<float> Break = new List<float>();
    private float iteration = .05f;
    private int listNum;
    private string objName;
    public int movementState;
    public float Val;
    public GameObject dropDirection;

    public bool done = false;

    // Start is called before the first frame update
    void Start()
    {
        // Get each individual limb in the player object
        objName = gameObject.name;
        switch(objName)
        {
            case "Base":
                listNum = 0;
                break;
            case "Lower Arm":
                listNum = 1;
                break;
            case "Upper Arm":
                listNum = 2;
                break;
            case "Wrist":
                listNum = 3;
                break;
            case "Hand":
                listNum = 4;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Set values once all movement is done
        if(listNum == 4)
        {
            done = true;
        }

        // Set values depending on direction of lower arm limb
        gameObject.transform.parent.GetComponent<Break>().doneCheck[listNum] = done;
        if (dropDirection.GetComponent<Break>().leftDrop == true)
        {
            Break = new List<float>{1.2f,0.8f,0f,0f,0f};
        } 
        else
        {
            Break = new List<float> {-1.2f,-0.8f,0f,0f,0f};
        }

        Move();
        MoveBack();
    }

    // Set original positions before executing drop
    public void Drop(){
        Val = gameObject.transform.parent.GetComponent<Break>().valStorage[listNum];
        movementState = 1;
    }

    // Move back to its original position before the break until each limb is at its set position
    void MoveBack()
    {
        if(movementState == 2)
        {
            // Set rise direction depending on direction of lower arm limb
            if(dropDirection.GetComponent<Break>().leftDrop == true)
            {
                // Move until in set position
                if (Val > gameObject.transform.parent.GetComponent<Break>().valStorage[listNum])
                {
                    Val -= iteration;
                }
                else
                {
                    Val = gameObject.transform.parent.GetComponent<Break>().valStorage[listNum];
                    movementState = 0;
                    done = true;
                }
            }
            else
            {
                if(Val < gameObject.transform.parent.GetComponent<Break>().valStorage[listNum])
                {
                    Val += iteration;
                }
                else
                {
                    Val = gameObject.transform.parent.GetComponent<Break>().valStorage[listNum];
                    movementState = 0;
                    done = true;
                }
            }
        }
    }

    // Do the break animation so long as each limb is not in its set break position
    void Move(){
        if(movementState == 1)
        {
            // Set fall direction depending on direction of lower arm limb
            if(dropDirection.GetComponent<Break>().leftDrop == true)
            {
                // Move until in set position
                if(Val<Break[listNum])
                {
                    Val += iteration;
                }
                else
                {
                    Val = Break[listNum];
                    movementState = 0;
                    done = false;
                }
            }
            else
            {
                if(Val>Break[listNum])
                {
                    Val -= iteration;
                }
                else
                {
                    Val = Break[listNum];
                    movementState = 0;
                    done = false;
                }
            }
        }
    }
}
