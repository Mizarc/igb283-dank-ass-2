﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFollow : MonoBehaviour
{
    public GameObject baseObj;
    public string objectName;

    
    // Start is called before the first frame update
    void Start()
    {
        // Get player's base object
        baseObj = GameObject.Find(objectName).transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // Set the base reference to follow the base
        transform.position = new Vector3(baseObj.transform.position.x,transform.position.y,transform.position.z);
    }
}
