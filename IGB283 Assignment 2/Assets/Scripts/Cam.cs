﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam : MonoBehaviour
{
    public Transform p1;
    public Transform p2;

    public Vector3 mid;

    public float distance;
    public GameObject cam;
    public Camera oCam;
    

    // Update is called once per frame
    void Update()
    {
        // Move the camera orthographically by the center point of the two player objects
        mid = (p1.position + p2.position)/2;
        distance = Vector3.Distance(p1.position,p2.position);
        cam.transform.position = new Vector3(mid.x, 3, -10);
        oCam.orthographicSize = 10+(distance/5);
    }
}
