﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed;
    public float jumpSpeed;
    public float longJumpSpeed;
    public float jumpHeight;
    public float longJumpHeight;
    public float bounds;
    public int jumpState;
    public GameObject reference;
    private bool isLeft;
    private bool isUp;
    private float startPos;
    private float maxHeight;
    private float speedStorage;
    private float distance;
    private GameObject player;
    private GameObject origin;
    private GameObject baseObj;
    private GameObject lowerObj;
    private GameObject upperObj;
    private GameObject wristObj;
    private GameObject handObj;


    // Start is called before the first frame update
    void Start()
    {
        // Set player object, speed, origin point, and jump status
        PlayerCheck();
        baseObj = gameObject.transform.GetChild(0).gameObject;
        lowerObj = gameObject.transform.GetChild(1).gameObject;
        upperObj = gameObject.transform.GetChild(2).gameObject;
        wristObj = gameObject.transform.GetChild(3).gameObject;
        handObj = gameObject.transform.GetChild(4).gameObject;
        isUp = true;
        speedStorage = speed;
        origin = GameObject.Find("Origin");
    }

    // Update is called once per frame
    void Update()
    {
        Controls();
        Direction();
        Jump(); 
        OriginDist();
    }

    // Gets player and player floor reference object by tag
    void PlayerCheck(){
        if(this.gameObject.tag == "Player1")
        {
            player = GameObject.FindWithTag("Player1");
            reference = GameObject.FindWithTag("Ref1");
        }
        else
        {
            player = GameObject.FindWithTag("Player2");
            reference = GameObject.FindWithTag("Ref2");
        }
    }
  
    void Jump()
    {
        // Move up until it reaches the height limit
        if (player.transform.position.y < maxHeight && isUp == true)
        {
            if (jumpState == 1)
            {
                player.transform.Translate(Vector2.up * jumpSpeed * Time.deltaTime);
            }
            else if (jumpState == 2)
            {
                player.transform.Translate(Vector2.up * longJumpSpeed * Time.deltaTime);
                speed = longJumpSpeed;
            }   
        }

        // Flip direction when height limit is reached
        else if (player.transform.position.y > maxHeight && jumpState != 0)
        {
            isUp = false;
        }

        // Move down until it reaches the floor limit
        if (player.transform.position.y > startPos && isUp == false)
        {
            if (jumpState == 1)
            {
                player.transform.Translate(Vector2.down * ( jumpSpeed * 1.5f) * Time.deltaTime);
            }
            else if (jumpState == 2)
            {
                player.transform.Translate(Vector2.down * ( longJumpSpeed * 1.5f) * Time.deltaTime);
            }
        }

        // Reset all values to before jump was triggered
        else if (player.transform.position.y < startPos && isUp == false && jumpState != 0)
        {
            player.transform.position = new Vector2(transform.position.x, startPos);
            speed = speedStorage;
            jumpState = 0;
            isUp = true;
        }
    }

    // Change movement direction
    void Direction()
    {
        if (isLeft)
        {
            player.transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
        else
        {
            player.transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
    }

    // Get distance from the center of the map to set boundaries
    void OriginDist(){
        origin.transform.position = new Vector3(origin.transform.position.x,reference.transform.position.y,origin.transform.position.z);
        distance = Vector3.Distance(origin.transform.position, reference.transform.position);
        // Flip direction when the player hits the boundary
        if (distance > bounds)
        {
            isLeft = !isLeft;
            
            // Manually set the object to the boundary position to prevent the
            // player to exceeding the boundary by spamming the directional key
            if(isLeft)
            {
                baseObj.transform.position = new Vector3(bounds,baseObj.transform.position.y,baseObj.transform.position.z);
                lowerObj.transform.position = new Vector3(bounds,lowerObj.transform.position.y,lowerObj.transform.position.z);
                upperObj.transform.position = new Vector3(bounds,upperObj.transform.position.y,upperObj.transform.position.z);
                wristObj.transform.position = new Vector3(bounds,wristObj.transform.position.y,wristObj.transform.position.z);
                handObj.transform.position = new Vector3(bounds,handObj.transform.position.y,handObj.transform.position.z);
            }
            else
            {
                baseObj.transform.position = new Vector3(-bounds,baseObj.transform.position.y,baseObj.transform.position.z);
                lowerObj.transform.position = new Vector3(-bounds,lowerObj.transform.position.y,lowerObj.transform.position.z);
                upperObj.transform.position = new Vector3(-bounds,upperObj.transform.position.y,upperObj.transform.position.z);
                wristObj.transform.position = new Vector3(-bounds,wristObj.transform.position.y,wristObj.transform.position.z);
                handObj.transform.position = new Vector3(-bounds,handObj.transform.position.y,handObj.transform.position.z);
            }
        }
    }

    // Set player controls
    void Controls()
    {
        // Player 1 Controls
        if(gameObject.tag == "Player1")
        {
            if (Input.GetKey("a"))
            {
                isLeft = true;
            }
            if (Input.GetKey("d"))
            {
                isLeft = false;
            }  
            if (Input.GetKey("w") && jumpState == 0)
            {
                startPos = player.transform.position.y;
                maxHeight = startPos + jumpHeight;
                jumpState = 1;
            }
            if (Input.GetKey("s") && jumpState == 0)
            {
                startPos = player.transform.position.y;
                maxHeight = startPos + longJumpHeight;
                jumpState = 2;
            }
        }

        // Player 2 Controls
        else if(gameObject.tag == "Player2")
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                isLeft = true;
            }  
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                isLeft = false;
            }
            if (Input.GetKey(KeyCode.UpArrow) && jumpState == 0)
            {
                startPos = player.transform.position.y;
                maxHeight = startPos + jumpHeight;
                jumpState = 1;
            }
            if (Input.GetKey(KeyCode.DownArrow) && jumpState == 0)
            {
                startPos = player.transform.position.y;
                maxHeight = startPos + longJumpHeight;
                jumpState = 2;
            }
        }
    }
}
