﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IGB283;

public class Limb : MonoBehaviour
{
    public GameObject child;
    public Vector3 jointLocation;
    public Vector3 jointOffset;
    public float angle;
    public float lastAngle;
    public Vector3[] limbVertexLocations;
    public Material material;
    public bool isDrop = false;

    private Mesh mesh;
    

    //Draw Function 
    public void DrawLimb()
    {
        //Add MeshFitler & MeshRenderer
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        mesh = GetComponent<MeshFilter>().mesh;
        GetComponent<MeshRenderer>().material = material;

        // Manually set hand shape, as it's more complex than the other shapes
        if(gameObject.tag == "Hand")
        {
            // Set vertex locations
            mesh.vertices = new Vector3[]
            {
                new Vector3(-0.29f,-0.29f),
                new Vector3(0.29f,-0.29f),
                new Vector3(0.50f,-0.14f),
                new Vector3(0.43f,-0.14f),
                new Vector3(0.14f,-0.14f),
                new Vector3(0.00f,0.00f),
                new Vector3(0.43f,0.00f),
                new Vector3(0.43f,0.14f),
                new Vector3(0.36f,0.14f),
                new Vector3(0.14f,0.14f),
                new Vector3(0.29f,0.29f),
                new Vector3(0.14f,0.29f),
                new Vector3(0.14f,1.00f),
                new Vector3(0.00f,0.86f),
                new Vector3(0.00f,0.29f),
                new Vector3(-0.07f,0.29f),
                new Vector3(-0.07f,1.00f),
                new Vector3(-0.21f,0.86f),
                new Vector3(-0.21f,0.29f),
                new Vector3(-0.29f,0.29f),
                new Vector3(-0.43f,0.71f),
                new Vector3(-0.43f,0.86f),
                new Vector3(-0.57f,0.71f),
                new Vector3(-0.43f,0.29f),
                new Vector3(-0.43f,0.14f),
            };

            // Set triangle faces using vertices
            mesh.triangles = new int[]
            {
                0,1,5,
                4,2,1,
                3,6,2,
                8,7,6,
                9,10,7,
                0,19,10,
                14,12,11,
                14,13,12,
                18,16,15,
                18,17,16,
                0,24,19,
                24,20,19,
                23,22,21,
            };
        }
        // Set other limb shapes
        else
        {
            mesh.vertices = limbVertexLocations;
            mesh.triangles = new int[] { 0, 1, 2, 0, 3, 2};
        }        
    }

    // Move limbs by the matrix transformation values
    public void MoveByOffset (Vector3 offset)
    {
        // Translation calculation
        Matrix3x3 T = Translate(offset);
        Vector3[] vertices = mesh.vertices;

        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = T.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;
        jointLocation = T.MultiplyPoint(jointLocation);

        // Move each child component
        if (child != null)
        {
            child.GetComponent<Limb>().MoveByOffset(offset);
        }
    }

    // Draw limbs before program starts
    void Awake()
    {
        DrawLimb();
    }

    // Move child component of the object if it exists
    void Start()
    {
        if (child != null)
        {
            child.GetComponent<Limb>().MoveByOffset(jointOffset);
        }
    }

    public void RotateAroundPoint(Vector3 point, float angle, float lastAngle)
    {
        // Calculations to rotate both around the its own axis and its orbit
        Matrix3x3 T1 = Translate(-point);
        Matrix3x3 R1 = Rotate(-lastAngle);
        Matrix3x3 T2 = Translate(point);
        Matrix3x3 R2 = Rotate(angle);
        Matrix3x3 M = T2 * R2 * R1 * T1;

        //Move the mesh using final matrix value
        Vector3[] vertices = mesh.vertices;
        for(int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = M.MultiplyPoint(vertices[i]);
        }

        mesh.vertices = vertices;

        jointLocation = M.MultiplyPoint(jointLocation);

        // Rotate each additional child component recurring
        if (child != null)
        {
            child.GetComponent<Limb>().RotateAroundPoint(point, angle, lastAngle);
        }

    }

    public void Update()
    {
        lastAngle = angle;
        isDrop = gameObject.transform.parent.GetComponent<Break>().isDrop;

        // Check if player is set to the drop position from the break script and use Animation2 value instead
        if (isDrop)
        {
            angle = gameObject.GetComponent<Animation2>().Val;
        }
        else
        {
           angle = gameObject.GetComponent<Animation1>().Val;
        }

        // Rotate the child joint using the angle values
        if (child != null)
        {
            child.GetComponent<Limb>().RotateAroundPoint(jointLocation, angle, lastAngle);
        }

        mesh.RecalculateBounds();
    }


    // Rotation function using a matrix
    public static Matrix3x3 Rotate (float angle)
    {
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(Mathf.Cos(angle), -Mathf.Sin(angle), 0.0f));
        matrix.SetRow(1, new Vector3(Mathf.Sin(angle), Mathf.Cos(angle), 0.0f));
        matrix.SetRow(2, new Vector3(0.0f, 0.0f, 1.0f));

        return matrix;

    }

    // Translation function using a matrix
    public static Matrix3x3 Translate (Vector3 offset)
    {
        Matrix3x3 matrix = new Matrix3x3();

        matrix.SetRow(0, new Vector3(1.0f, 0.0f, offset.x));
        matrix.SetRow(1, new Vector3(0.0f, 1.0f, offset.y));
        matrix.SetRow(2, new Vector3(0.0f, 0.0f, 1.0f));

        return matrix;
    }



}
