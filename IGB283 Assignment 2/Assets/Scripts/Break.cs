﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Break : MonoBehaviour
{
    public string key;
    public List<float> valStorage;

    public bool leftDrop;

    public bool isDrop;
    public float cooldown;
    public float remaining;
    private bool trigger;
    
    public bool[] doneCheck = new bool[5];


    // Update is called once per frame
    void Update()
    {
        CheckButton();
        Resume();
    }

    void CheckButton()
    {
        // Check if the break key is pressed and if not already in break position
        if(Input.GetKeyDown(key) && isDrop == false && gameObject.GetComponent<Movement>().jumpState == 0)
        {
            // Set triggers to put it in the break position for x amount of time, while retaining
            // Original values before the break to be able to set it back to its original position
            trigger = true;
            remaining = Time.time + cooldown;
            isDrop = true;
            valStorage.Clear();
            CheckDirection();

            // Set values for every child object
            for(int i = 0; i < 4; i += 1)
            {
                valStorage.Add(gameObject.transform.GetChild(i).gameObject.GetComponent<Animation1>().Val);
                gameObject.transform.GetChild(i).gameObject.GetComponent<Animation1>().enabled = false;
                gameObject.transform.GetChild(i).gameObject.GetComponent<Animation2>().Drop();
                gameObject.transform.GetChild(i).gameObject.GetComponent<Limb>().isDrop = true;
                gameObject.GetComponent<Movement>().enabled = false;
            }
        }
    }

    // Resume original movement
    void Resume()
    {
        if (Time.time > remaining && trigger == true)
        {
            // Set all child objects to the break movement state
            gameObject.GetComponent<Movement>().enabled = true;
            for(int i = 0; i < 4; i += 1)
            {
                gameObject.transform.GetChild(i).gameObject.GetComponent<Animation2>().movementState = 2;
            }

            // Set original values to continue doing its original animation
            if(!Array.Exists(doneCheck, element => element == false))            
            {
                for(int i = 0; i < 4; i += 1)
                {
                    gameObject.transform.GetChild(i).gameObject.GetComponent<Animation1>().enabled = true;        
                }
                isDrop = false;
                trigger = false;
            }
        }
    }

    // Drops player in the direction that they were facing when the button was pressed
    void CheckDirection()
    {
        if(gameObject.transform.GetChild(0).gameObject.GetComponent<Animation1>().Val < 0)
        {
            leftDrop = false;
        }
        else{
            leftDrop = true;
        }
    }
}
