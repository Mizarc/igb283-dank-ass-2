﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation1 : MonoBehaviour
{
    public float minVal;
    public float maxVal;
    public float Val = 0f;
    public float iteration;
    public bool isLeft;

    
    // Start is called before the first frame update
    void Start()
    {
        isLeft = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Constantly run the animation based on values set in Unity
        if (isLeft){
            Val += iteration;
        }
        else
        {
            Val -= iteration;
        }

        if (Val > maxVal)
        {
            isLeft = false; 
        }
        else if (Val < minVal)
        {
            isLeft = true;
        }
    }
}
